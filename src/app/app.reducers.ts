import * as tracks from './store/tracks.reducers';
import { ActionReducerMap } from '@ngrx/store';

export interface AppState {
    tracks: tracks.State;
}

export const reducers: ActionReducerMap<AppState> = {
    tracks: tracks.reducer
};