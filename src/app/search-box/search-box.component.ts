import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { NgxSpinnerService } from 'ngx-spinner';

import { Store } from '@ngrx/store';
import { AppState } from "../app.reducers";
import * as TracksActions from '../store/tracks.actions';

@Component({
    selector: 'search-box',
    templateUrl: './search-box.component.html',
    styleUrls: ['./search-box.component.scss']
})
export class SearchBoxComponent implements OnInit {

    searchForm: FormGroup;

    constructor(
        private fb: FormBuilder,
        private store: Store<AppState>,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.searchForm = this.fb.group({
            searchField: ['', Validators.required]
        });
    }

    search() {
        this.spinner.show();
        this.store.dispatch(
            new TracksActions.Search(this.searchForm.value.searchField)
        );
    }
}
