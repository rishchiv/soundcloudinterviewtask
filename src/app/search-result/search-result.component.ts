import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

import { Observable } from 'rxjs';

import { Store } from '@ngrx/store';
import { AppState } from "../app.reducers";
import * as TracksActions from '../store/tracks.actions';
import { take } from 'rxjs/operators';

@Component({
    selector: 'search-result',
    templateUrl: './search-result.component.html',
    styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent implements OnInit {

    nextUrl: string = null;
    trackState: Observable<AppState["tracks"]>;

    constructor(
        private store: Store<AppState>,
        private spinner: NgxSpinnerService
    ) { }

    ngOnInit() {
        this.trackState = this.store.select('tracks');
    }

    getNextTracks() {
        this.spinner.show();
        this.trackState.pipe(take(1))
            .subscribe(state => {
                this.nextUrl = state.nextUrl;
            })
        this.store.dispatch(
            new TracksActions.Search(this.nextUrl)
        );
    }
}
