export interface Track {
    name: string,
    artist: string,
    imageUrl: string
}