import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { NgxSpinnerModule } from 'ngx-spinner';

import { AppComponent } from './app.component';
import { TrackItemComponent } from './track-item/track-item.component';
import { SearchBoxComponent } from './search-box/search-box.component';
import { SearchResultComponent } from './search-result/search-result.component';
import { TracksService } from './services/tracks.service';

import { StoreModule } from "@ngrx/store";
import { reducers } from './app.reducers';
import { EffectsModule } from '@ngrx/effects';
import { TracksEffects } from './store/tracks.effects';

@NgModule({
  declarations: [
    TrackItemComponent,
    SearchBoxComponent,
    SearchResultComponent,
    AppComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    BrowserModule,
    HttpClientModule,
    NgxSpinnerModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([TracksEffects]),
  ],
  providers: [TracksService],
  bootstrap: [AppComponent]
})
export class AppModule { }
