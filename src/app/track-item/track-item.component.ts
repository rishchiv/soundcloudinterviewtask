import { Component, Input } from '@angular/core';
import { Track } from '../models/track.model';

@Component({
    selector: 'track-item',
    templateUrl: './track-item.component.html',
    styleUrls: ['./track-item.component.scss']
})
export class TrackItemComponent {

    @Input() track: Track;

    constructor() { }

    getTrackImage(track: Track) {
        if (track.imageUrl != null) {
            return track.imageUrl;
        }
        return 'assets/images/artist_placeholder.png';
    }
}
