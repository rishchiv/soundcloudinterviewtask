import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map, catchError, switchMap } from 'rxjs/operators';

import { NgxSpinnerService } from 'ngx-spinner';

import { Action } from '@ngrx/store';
import * as TracksActions from './tracks.actions';
import { TracksService } from '../services/tracks.service';

import { Track } from '../models/track.model';

@Injectable()
export class TracksEffects {

    constructor(
        private actions$: Actions,
        private tracksService: TracksService,
        private spinner: NgxSpinnerService
    ) { }

    @Effect()
    Search: Observable<Action> = this.actions$.pipe(
        ofType(TracksActions.TracksActionTypes.SEARCH),
        map((action: TracksActions.Search) => action.payload),
        switchMap(payload => this.handleTrackRequests(payload)),
        map(response => this.succefullHandler(response)),
        catchError(error => this.errorHandler(error))
    );

    private handleTrackRequests(payload): Observable<Object> {
        if (payload.includes(this.tracksService.baseUrl))
            return this.tracksService.getNextPage(payload);
        else
            return this.tracksService.search(payload);
    }

    private succefullHandler(response) {
        this.spinner.hide();
        return new TracksActions.Success({
            nextUrl: response['next_href'],
            tracks: this.modifyTracksCollection(response['collection'])
        });
    }

    private errorHandler(error) {
        this.spinner.hide();
        return of(new TracksActions.Failure(error.message));
    }

    private modifyTracksCollection(tracksCollection): Track[] {
        return tracksCollection.map(trackObj => {
            let track: Track = <Track>{};
            track.name = trackObj.title;
            track.artist = trackObj.user.username;
            track.imageUrl = trackObj.artwork_url;
            return track;
        });
    }
}