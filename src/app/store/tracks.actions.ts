import { Action } from '@ngrx/store';
import { Track } from '../models/track.model';

export enum TracksActionTypes {
    SEARCH = 'Search',
    SUCCESS = 'Successful request',
    FAILURE = 'Failed request'
}

export class Search implements Action {
    readonly type = TracksActionTypes.SEARCH;
    constructor(public payload: string) { }
}

export class Success implements Action {
    readonly type = TracksActionTypes.SUCCESS;
    constructor(public payload: { nextUrl: string, tracks: Track[] }) { }
}

export class Failure implements Action {
    readonly type = TracksActionTypes.FAILURE;
    constructor(public payload: string) { }
}

export type TracksActions =
    | Search
    | Success
    | Failure