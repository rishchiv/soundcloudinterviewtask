import * as TracksActions from './tracks.actions';

import { Track } from '../models/track.model';

export interface State {
    nextUrl: string;
    tracks: Track[] | null;
    // error message
    errorMessage: string | null;
}

export const initialState: State = {
    nextUrl: null,
    tracks: null,
    errorMessage: null
};

export function reducer(state = initialState, action: TracksActions.TracksActions): State {
    switch (action.type) {
        case TracksActions.TracksActionTypes.SUCCESS:
            return {
                ...state,
                nextUrl: action.payload.nextUrl,
                tracks: action.payload.tracks,
                errorMessage: null
            };
        case TracksActions.TracksActionTypes.FAILURE:
            return {
                ...state,
                errorMessage: action.payload
            };
        default:
            return state;
    }
}