import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class TracksService {

    baseUrl: string = 'https://api.soundcloud.com';
    private clientId: string = '8e1349e63dfd43dc67a63e0de3befc68';

    constructor(private http: HttpClient) { }

    search(keyword: string): Observable<Object> {
        let uri = this.makeSearchUri(keyword);
        return this.http.get(uri);
    }

    getNextPage(uri: string): Observable<Object> {
        return this.http.get(uri);
    }

    private makeSearchUri(keyword: string): string {
        return `${this.baseUrl}/tracks?linked_partitioning=1&client_id=${this.clientId}&q=${keyword}&limit=5`
    }
}
